# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool

from . import bom, dash, product, production, restaurant, sale, shop
from . import invoice as invoice


def register():
    Pool.register(
        restaurant.SaleShopTable,
        restaurant.SaleShopTableZone,
        restaurant.Reservation,
        sale.Sale,
        sale.SaleForceDraft,
        sale.SaleLine,
        sale.SaleLineHistoryDelete,
        sale.SaleLineHistoryDeleteStart,
        sale.SaleMove,
        shop.SaleShop,
        bom.BOM,
        production.TaskPrinter,
        production.ConfigurationTask,
        production.Task,
        product.Product,
        dash.DashApp,
        dash.AppRestOrder,
        module='sale_pos_frontend_rest', type_='model')
    Pool.register(
        sale.SaleSquareBoxGlobal,
        sale.SaleLineHistoryDeleteWizard,
        module='sale_pos_frontend_rest', type_='wizard')
    Pool.register(
        sale.SaleSquareBoxGlobalReport,
        sale.SaleLineHistoryDeleteReport,
        module='sale_pos_frontend_rest', type_='report')
