# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'

    tasks = fields.One2Many('production.configuration_task', 'product', 'Tasks')

    @classmethod
    def copy(cls, records, default=None):
        default = {} if default is None else default.copy()
        default.setdefault('tasks', None)
        return super(Product, cls).copy(records, default=default)
