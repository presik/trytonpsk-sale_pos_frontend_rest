# This file is part sale_shop module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.model import DeactivableMixin, ModelSQL, ModelView, fields
from trytond.pyson import Eval


class SaleShopTable(ModelSQL, ModelView, DeactivableMixin):
    "Sale Shop Table"
    __name__ = 'sale.shop.table'
    shop = fields.Many2One('sale.shop', 'Shop', required=True)
    zone = fields.Many2One('shop.table.zone', 'Zone')
    name = fields.Char('Table Name', required=True)
    # active = fields.Boolean('Active')
    capacity = fields.Integer('Capacity', required=True)
    sale = fields.Many2One('sale.sale', 'Sale')
    state = fields.Selection([
        ('available', 'Available'),
        ('occupied', 'Occupied'),
        ('reserved', 'Reserved'),
        ], 'State', states={'readonly': False},
    )

    @staticmethod
    def default_active():
        return True

    @staticmethod
    def default_state():
        return 'available'


class SaleShopTableZone(ModelSQL, ModelView, DeactivableMixin):
    "Sale Shop Table Zone"

    __name__ = 'shop.table.zone'

    shop = fields.Many2One('sale.shop', 'Shop', required=True)
    name = fields.Char('Zone Name', required=True)
    code = fields.Char('Code')
    tables = fields.One2Many('sale.shop.table', 'zone', 'Tables', add_remove=[
        ('shop', '=', Eval('shop')),
        ])


class Reservation(ModelSQL, ModelView):
    "Sale Reservation"
    __name__ = 'sale.reservation'
