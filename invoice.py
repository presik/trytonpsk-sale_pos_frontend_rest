# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.pyson import Eval

KIND = [
    ('', ''),
    ('take_away', 'Take Away'),
    ('delivery', 'Delivery'),
    ('to_table', 'To Table')
]


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'
    sale_kind = fields.Selection(KIND, 'Sale Kind',
            states={'invisible': Eval('type') != 'out'}
    )
