# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class SaleShop(metaclass=PoolMeta):
    __name__ = 'sale.shop'
    tables = fields.One2Many('sale.shop.table', 'shop', 'Tables')
    zones = fields.One2Many('shop.table.zone', 'shop', 'Zones')
    local_printer = fields.Char('Local Printer Server',
        help="Example: http://192.168.0.122:7001")
