# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
# from trytond.pyson import Eval
from datetime import datetime, timedelta
from decimal import Decimal

from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction

# from trytond.exceptions import UserError
# from trytond.i18n import gettext


def round_dec(number):
    return Decimal(number.quantize(Decimal('.01')))


STATES = {'required': True}

# for remove
# class WorkStation(ModelSQL, ModelView):
#     "Work Station"
#     __name__ = 'production.workstation'

#     name = fields.Char('Name', states=STATES)
#     code = fields.Char('Code', states=STATES)
#     printers = fields.Many2Many('production.workstation.pos_printer', 'work_station', 'printer', 'Printers')

# for remove
# class WorkStationPrinter(ModelSQL, ModelView):
#     "Work Station"
#     __name__ = 'production.workstation.pos_printer'

#     work_station = fields.Many2One('production.workstation', 'Work Station')
#     printer = fields.Many2One('sale.pos_printer', 'Printer')


class TaskPrinter(ModelSQL, ModelView):
    "Task Printer"
    __name__ = 'production.task.pos_printer'

    task = fields.Many2One('production.configuration_task', 'Tasks')
    printer = fields.Many2One('sale.pos_printer', 'Printer')


class ConfigurationTask(ModelSQL, ModelView):
    "Configuration Task"
    __name__ = 'production.configuration_task'

    name = fields.Char('Name', states=STATES)
    description = fields.Text('Description')
    product = fields.Many2One('product.product', 'Product',
        search_context={
            'outputs': Eval('_parent_ldm', {}).get('outputs')})
    printers = fields.Many2Many('production.task.pos_printer', 'task',
        'printer', 'Printers', states=STATES)
    # for remove
    # ldm = fields.Many2One('production.bom', 'LDM', states=STATES)
    # work_station = fields.Many2One('production.workstation', 'Work Station', states=STATES)

    @classmethod
    def __register__(cls, module_name):
        super(ConfigurationTask, cls).__register__(module_name)

        table = cls.__table_handler__(module_name)
        if table.column_exist('ldm'):
            TaskPrinter = Pool().get('production.task.pos_printer')
            query = """select t.id, p.printer from
                production_configuration_task as t
                join production_workstation_pos_printer as p
                on p.work_station=t.work_station"""
            cursor = Transaction().connection.cursor()
            cursor.execute(query)
            res = cursor.fetchall()
            to_create = [{'task': t, 'printer': p} for t, p in res]
            TaskPrinter.create(to_create)
            table.drop_column('ldm')
            table.drop_column('work_station')


class Task(ModelSQL, ModelView):
    "Task"
    __name__ = 'production.task'

    name = fields.Char('Name', states=STATES)
    note = fields.Text('Note')
    line = fields.Many2One('sale.line', 'Sale Line',
        states={'readonly': True}, ondelete="CASCADE")
    quantity = fields.Integer('Quantity')
    state = fields.Selection([
        ('pending', 'Pending'),
        ('commanded', 'Commanded'),
        ('done', 'Done')], 'State')
    printer = fields.Many2One('sale.pos_printer', 'Printer')
    task = fields.Many2One('production.configuration_task', 'Task')
    sale_number = fields.Char('Sale Number')
    # ldm = fields.Many2One('production.bom', 'LDM', states=STATES)

    @classmethod
    def __setup__(cls):
        super(Task, cls).__setup__()

    @classmethod
    def get_tasks_to_print(cls, args):
        thirty_minutes_ago = str(datetime.now() - timedelta(minutes=30))
        dom = [
            ('state', '=', 'pending'),
            ('create_date', '>=', thirty_minutes_ago),
        ]
        if args.get('sale_id'):
            dom.append(('line.sale', '=', args['sale_id']))

        fields = [
            'name', 'note', 'quantity', 'sale_number',
            'line', 'line.sale',
            'printer.name', 'printer.shop', 'printer.host',
            'printer.interface', 'printer.port', 'printer.row_characters',
        ]
        tasks = cls.search_read(
                dom,
                fields_names=fields,
                order=[('id', 'DESC')])
        tasks_print = {}
        for t in tasks:
            if not t['line.']:
                continue
            printer_id = t['printer.']['id']
            sale_id = t['line.']['sale']
            key = str(printer_id) + '_' + str(sale_id)
            line = {
                'quantity': t['quantity'],
                'name': t['name'],
                'line': t['line.']['id'],
                'task': t['id'],
                'note': t['note'],
            }
            try:
                tasks_print[key]['lines'].append(line)
            except Exception:
                tasks_print[key] = {}
                tasks_print[key]['lines'] = [line]
                value_printer = t['printer.']
                value_printer['device'] = value_printer['host']
                value_printer['profile'] = ''
                tasks_print[key]['printer'] = t['printer.']
                tasks_print[key]['sale'] = t['sale_number']
                tasks_print[key]['work_station'] = value_printer['name']
        return list(tasks_print.values())
