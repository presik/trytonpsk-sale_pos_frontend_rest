# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from decimal import Decimal


def round_dec(number):
    return Decimal(number.quantize(Decimal('.01')))


class BOM(metaclass=PoolMeta):
    __name__ = 'production.bom'

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('output_products', None)
        return super(BOM, cls).copy(records, default=default)
